/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/netanim-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/flow-monitor-module.h"



// Part 2 - Wifi network with 2, 4 or 6 devices without mobility
// Default Network Topology
//
//   Wifi 10.1.2.0
//                  AP
//       *     *    *
//       |     |    |    10.1.1.0
//       n3    n2   n1 -------------- n0
//                          csma
//

using namespace ns3;
uint32_t nWifi = 2;
bool tracing = false;
uint16_t port = 9;
float simul_time = 30.0;
uint16_t traffic = 0; //0 for UDP, 1 for TCP and 2 for 50% each
NS_LOG_COMPONENT_DEFINE ("project-part2");

void transmit_udp(Ipv4InterfaceContainer csmaInterfaces, NodeContainer serverNode, NodeContainer wifiStaNodes)
{
    /*
        Generate the applications to transmit CBR traffic from the sta nodes to the server trough UDP only
        The frame sizes are 512 bytes
    */


  //Server
  PacketSinkHelper sink ("ns3::UdpSocketFactory", InetSocketAddress (csmaInterfaces.GetAddress(0), port));
  ApplicationContainer server = sink.Install (serverNode);
  server.Start (Seconds (0.0));
  server.Stop (Seconds (simul_time));

  //CBR senders
  for (uint32_t i = 0; i < wifiStaNodes.GetN (); ++i) {
     OnOffHelper onoff ("ns3::UdpSocketFactory", InetSocketAddress (csmaInterfaces.GetAddress (0), port));
     onoff.SetConstantRate (DataRate ("512kbps"), 512-28);
     onoff.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
     onoff.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
     ApplicationContainer clientApps = onoff.Install (wifiStaNodes.Get (i));
     clientApps.Start (Seconds (1.0));
     clientApps.Stop (Seconds (simul_time));
   }
}


void transmit_tcp(Ipv4InterfaceContainer csmaInterfaces, NodeContainer serverNode,   NodeContainer wifiStaNodes)
{
    /**
        Generate the applications to transmit burst traffic from the sta nodes to the server trough TCP only
        The frame sizes are 1500 bytes
    **/
  //Server


  PacketSinkHelper sink ("ns3::TcpSocketFactory", InetSocketAddress (csmaInterfaces.GetAddress(0), port));
  ApplicationContainer server = sink.Install (serverNode);
  server.Start (Seconds (0.0));
  server.Stop (Seconds (simul_time));

  //Burst senders
  for (uint32_t i = 0; i < wifiStaNodes.GetN (); ++i) {

      OnOffHelper onoff ("ns3::TcpSocketFactory", InetSocketAddress (csmaInterfaces.GetAddress (0), port));
      onoff.SetConstantRate (DataRate ("512kbps"), 1500-28);
      onoff.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
      onoff.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
      ApplicationContainer clientApps = onoff.Install (wifiStaNodes.Get (i));
      clientApps.Start (Seconds (1.0));
      clientApps.Stop (Seconds (simul_time));
  }
}

void transmit_udp_tcp(Ipv4InterfaceContainer csmaInterfaces, NodeContainer serverNode, NodeContainer wifiStaNodes)
{
  /**
        Generate the applications to transmit CBR and burst traffic from the sta nodes to the server trough UDP and TCP respectively
    **/
  //Server UDP
  PacketSinkHelper sink ("ns3::UdpSocketFactory", InetSocketAddress (csmaInterfaces.GetAddress(0), port));
  ApplicationContainer server = sink.Install (serverNode);
  server.Start (Seconds (0.0));
  server.Stop (Seconds (simul_time/2.0));

  //Server TCP
  PacketSinkHelper sink2 ("ns3::TcpSocketFactory", InetSocketAddress (csmaInterfaces.GetAddress(0), port));
  ApplicationContainer server2 = sink2.Install (serverNode);
  server2.Start (Seconds (simul_time/2.0));
  server2.Stop (Seconds (simul_time));

  //CBR senders
  for (uint32_t i = 0; i < wifiStaNodes.GetN (); ++i) {
      OnOffHelper onoff ("ns3::UdpSocketFactory", InetSocketAddress (csmaInterfaces.GetAddress (0), port));
      onoff.SetConstantRate (DataRate ("512kbps"), 512-28);
      onoff.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
      onoff.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
      ApplicationContainer clientApps = onoff.Install (wifiStaNodes.Get (i));
      clientApps.Start (Seconds (1.0));
      clientApps.Stop (Seconds (simul_time/2.0));
  }

  //Burst senders
    for (uint32_t i = 0; i < wifiStaNodes.GetN (); ++i) {
      OnOffHelper onoff2 ("ns3::TcpSocketFactory", InetSocketAddress (csmaInterfaces.GetAddress (0), port));
      onoff2.SetConstantRate (DataRate ("512kbps"), 1500-28);
      onoff2.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
      onoff2.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
      ApplicationContainer clientApps2 = onoff2.Install (wifiStaNodes.Get (i));
      clientApps2.Start (Seconds (simul_time/2.0));
      clientApps2.Stop (Seconds (simul_time));
    }
}




int
main (int argc, char *argv[])
{

  CommandLine cmd;
  cmd.AddValue ("tracing", "Enable pcap tracing", tracing);
  cmd.AddValue("traffic", "(0) UDP only; (1) TCP only; (2) Half UDP, half TCP", traffic);
  cmd.AddValue("nWifi", "Number of wifi devices", nWifi);

  cmd.Parse (argc,argv);


  //csma config
  NodeContainer csmaNodes;
  csmaNodes.Create (2);

  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
  csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));
  //csma.SetChannelAttribute ("Delay", StringValue ("2ms"));


  NetDeviceContainer csmaDevices;
  csmaDevices = csma.Install (csmaNodes);
  NodeContainer serverNode = csmaNodes.Get(0);

  //Wifi config
  NodeContainer wifiStaNodes;
  wifiStaNodes.Create (nWifi);
  NodeContainer wifiApNode = csmaNodes.Get (1);

  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  //channel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  //channel.AddPropagationLoss("ns3::LogDistancePropagationLossModel");
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ());


  WifiHelper wifi;
  wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

  WifiMacHelper mac;
  Ssid ssid = Ssid ("ns-3-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid),
               "ActiveProbing", BooleanValue (false));

  NetDeviceContainer staDevices;
  staDevices = wifi.Install (phy, mac, wifiStaNodes);

  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));

  NetDeviceContainer apDevices;
  apDevices = wifi.Install (phy, mac, wifiApNode);

  MobilityHelper mobility;

  Ptr<ListPositionAllocator> positionAllocSta = CreateObject<ListPositionAllocator> ();
  positionAllocSta->Add (Vector (1.0, 0.0, 0.0));
  mobility.SetPositionAllocator (positionAllocSta);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiStaNodes);

   for (uint32_t i = 0; i < wifiStaNodes.GetN (); ++i) {
    AnimationInterface::SetConstantPosition (wifiStaNodes.Get (i), i+1, i+1);
   }


  Ptr<ListPositionAllocator> positionAllocAP = CreateObject<ListPositionAllocator> ();
  positionAllocAP->Add (Vector (0.0, 0.0, 0.0));
  mobility.SetPositionAllocator (positionAllocAP);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiApNode);


  AnimationInterface::SetConstantPosition (csmaNodes.Get (0), 0, 3);
  AnimationInterface::SetConstantPosition (csmaNodes.Get (1), 0, 0);


  InternetStackHelper stack;
  stack.Install(serverNode);
  stack.Install (wifiApNode);
  stack.Install (wifiStaNodes);

  Ipv4AddressHelper address;

  address.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer csmaInterfaces;
  csmaInterfaces = address.Assign (csmaDevices);


  address.SetBase ("10.1.2.0", "255.255.255.0");
  address.Assign (staDevices);
  address.Assign (apDevices);

 if(traffic == 0)
    transmit_udp(csmaInterfaces, serverNode, wifiStaNodes);
  else if(traffic == 1)
    transmit_tcp(csmaInterfaces, serverNode, wifiStaNodes);
  else
    transmit_udp_tcp(csmaInterfaces, serverNode, wifiStaNodes);

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  Simulator::Stop (Seconds (simul_time));



  if (tracing == true)
    {
      csma.EnablePcapAll ("pcaps/part2/trace-csma-project-part2");
      phy.EnablePcap ("pcaps/part2/trace-ap-wifi-project-part2", apDevices.Get (0));
      phy.EnablePcap ("pcaps/part2/trace-sta-wifi-project-part2", staDevices);
    }

 //Animation
  AnimationInterface anim ("project-animation-part2.xml"); // Mandatory
  for (uint32_t i = 0; i < wifiStaNodes.GetN (); ++i)
    {
      anim.UpdateNodeDescription (wifiStaNodes.Get (i), "STA"); // Optional
      anim.UpdateNodeColor (wifiStaNodes.Get (i), 255, 0, 0); // Optional
    }
  for (uint32_t i = 0; i < wifiApNode.GetN (); ++i)
    {
      anim.UpdateNodeDescription (wifiApNode.Get (i), "AP"); // Optional
      anim.UpdateNodeColor (wifiApNode.Get (i), 0, 255, 0); // Optional
    }
   for (uint32_t i = 0; i < serverNode.GetN (); ++i)
    {
      anim.UpdateNodeDescription (serverNode.Get (i), "Server"); // Optional
      anim.UpdateNodeColor (serverNode.Get (i), 0, 255, 255); // Optional
    }

  anim.EnablePacketMetadata (); // Optional
  anim.EnableIpv4RouteTracking ("routingtable-wireless-part2.xml", Seconds (0), Seconds (5), Seconds (0.25)); //Optional
  anim.EnableWifiMacCounters (Seconds (0), Seconds (10)); //Optional
  anim.EnableWifiPhyCounters (Seconds (0), Seconds (10)); //Optional


  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();

  Simulator::Run ();

  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  monitor->CheckForLostPackets();

  ns3::Time delay;
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
  {
	  Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
          std::cout << "Flow " << i->first  << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
          //std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
          //std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
      	  std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / (i->second.timeLastRxPacket.GetSeconds() - i->second.timeFirstTxPacket.GetSeconds())/1024/1024  << " Mbps\n";
      	  std::cout << " Lost packets = " << i->second.lostPackets  << "\n";
      	  //std::cout << " Delay Sum= " << i->second.delaySum  << "\n";
      	  if(traffic == 0){
            delay = i->second.delaySum/(i->second.txBytes/512);
          }
          else if(traffic == 1){
            delay = i->second.delaySum/(i->second.txBytes/1500);
          }
          else{
            if(i->first==1){
                delay = i->second.delaySum/(i->second.txBytes/512);
            }
            else {
                delay = i->second.delaySum/(i->second.txBytes/1500);
            }
          }
      	  std::cout << " Avg Delay = " << delay  << "\n";

  }

  monitor->SerializeToXmlFile("flow/project-part2.flowmon", true, true);


  Simulator::Destroy ();

  return 0;
}
